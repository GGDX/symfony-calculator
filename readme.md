# Code test: Symfony calculator

### Installation

Clone the repository and install dependencies

```bash
git clone git@gitlab.com:GGDX/symfony-calculator.git
cd symfony-calculator
composer install
```

### Run

Using Symfony server

`symfony server:start`

Using PHP server

`cd public && php -S 127.0.0.1:8000`

### Tests

From the project root, `php bin/phpunit`
