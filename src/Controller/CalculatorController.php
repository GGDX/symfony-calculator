<?php

namespace App\Controller;

use App\Utilities\Calculator\Calculator;
use App\Utilities\Calculator\Exceptions\BadOperatorException;
use App\Utilities\Calculator\Exceptions\MoreThanOneSecondNumberProvidedException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CalculatorController extends AbstractController
{
    /**
     * @var array
     */
    protected $operators;

    public function __construct()
    {
        $this->operators = (new Calculator)->getOperators();
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        return $this->render('pages/index.html.twig', [
            'operators' => $this->operators,
        ]);
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function calculate(Request $request)
    {
        $viewData = ['operators' => $this->operators];
        $validator = $this->validate($request);
        if($validator !== true) {
            $viewData['errors'] = $validator;
        } else {
            $operator = $request->get('operator');

            $calculator = new Calculator($request->get('first_number'));
            $sum = $calculator->$operator($request->get('second_number'));
            $viewData['result'] = $request->get('first_number') . ' ' . $operator . ' ' . $request->get('second_number') . ' = ' .  $sum;
        }

        return $this->render('pages/index.html.twig', $viewData);
    }

    /**
     * Basic validation
     * 
     * @param Symfony\Component\HttpFoundation\Request
     * @return void
     */
    protected function validate(Request $request)
    {
        $errors = [];

        if($request->get('first_number') == null) {
            $errors[] = 'The first number is required.';
        } else {
            if(!is_numeric($request->get('first_number'))) {
                $errors[] = 'The first number must be numeric.';
            }
        }
        
        if($request->get('second_number') !== null && !is_numeric($request->get('second_number'))) {
            $errors[] = 'The second number must be numeric.';
        }

        if($request->get('operator') == null) {
            $errors[] = 'The operator is required.';
        } else {
            // die(dump([$request->get('operator'), (new Calculator)->getOperators()]));
            if(!in_array($request->get('operator'), (new Calculator)->getOperators())) {
                $errors[] = 'The operator is invalid.';
            }
        }

        return count($errors) ? $errors : true;
    }
}
