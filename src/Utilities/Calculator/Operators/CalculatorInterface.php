<?php namespace App\Utilities\Calculator\Operators;

interface CalculatorInterface
{
    /**
     * Perform calculation
     * 
     * @param int|float $a
     * @param int|float $b
     * @return mixed
     */
    public function calculate($a, $b);
}