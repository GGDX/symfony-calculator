<?php namespace App\Utilities\Calculator\Operators;

class Division implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public function calculate($a, $b)
    {
        return $a / $b;
    }
}