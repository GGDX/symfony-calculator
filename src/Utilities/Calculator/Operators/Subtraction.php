<?php namespace App\Utilities\Calculator\Operators;

class Subtraction implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public function calculate($a, $b)
    {
        return $a - $b;
    }
}