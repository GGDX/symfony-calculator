<?php namespace App\Utilities\Calculator\Operators;

// And is a reserved keyword
class Also implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public function calculate($a, $b)
    {
        return $a && $b;
    }
}