<?php namespace App\Utilities\Calculator\Operators;

class ExclusiveOr implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public function calculate($a, $b)
    {
        return $a xor $b;
    }
}