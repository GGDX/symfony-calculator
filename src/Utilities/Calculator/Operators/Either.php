<?php namespace App\Utilities\Calculator\Operators;

class Either implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public function calculate($a, $b)
    {
        return $a || $b;
    }
}