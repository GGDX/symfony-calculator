<?php namespace App\Utilities\Calculator\Operators;

class Addition implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public function calculate($a, $b)
    {
        return $a + $b;
    }
}