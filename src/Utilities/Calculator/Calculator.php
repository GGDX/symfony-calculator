<?php namespace App\Utilities\Calculator;

use App\Utilities\Calculator\Exceptions\BadOperatorException;
use App\Utilities\Calculator\Exceptions\MoreThanOneSecondNumberProvidedException;

class Calculator
{

    /**
     * @var int|float
     */
    protected $first_number;

    /**
     * @var int|float
     */
    protected $second_number;

    /**
     * @var array
     */
    protected $operations_map = [
        'add' => \App\Utilities\Calculator\Operators\Addition::class,
        'subtract' => \App\Utilities\Calculator\Operators\Subtraction::class,
        'multiply' => \App\Utilities\Calculator\Operators\Multiplication::class,
        'divide' => \App\Utilities\Calculator\Operators\Division::class,
        'and' => \App\Utilities\Calculator\Operators\Also::class,
        'or' => \App\Utilities\Calculator\Operators\Either::class,
        'xor' => \App\Utilities\Calculator\Operators\ExclusiveOr::class
    ];

    /**
     * @param int|float $first_number
     * @return void
     */
    public function __construct($first_number = 0)
    {
        $this->first_number = $first_number;
    }

    /**
     * Provide available operators
     * 
     * @return array
     */
    public function getOperators(): array
    {
        return array_keys($this->operations_map);
    }

    /**
     * Perform calcuation
     * 
     * @param string $method    Operator
     * @param array $args       Numeric value
     * @return mixed 
     */
    public function __call($method, $args)
    {
        $calculation = new $this->operations_map[$method]();
        $result = $calculation->calculate($this->first_number, $args[0]);

        return is_bool($result) ? (int) $result : $result;
    }
}