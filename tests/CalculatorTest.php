<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Utilities\Calculator\Calculator;

class CalculatorTest extends TestCase
{

    /**
     * Test Addition
     * 
     * @return void
     */
    public function testCalculatorAdd()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->add(3);

        $this->assertEquals($sum, 8);
    }

    /**
     * Test Addition
     * 
     * @return void
     */
    public function testCalculatorSubtract()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->subtract(3);

        $this->assertEquals($sum, 2);
    }

    /**
     * Test Division
     * 
     * @return void
     */
    public function testCalculatorDivide()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->divide(2);

        $this->assertEquals($sum, 2.5);
    }

    /**
     * Test Multiplication
     * 
     * @return void
     */
    public function testCalculatorMultiply()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->multiply(3);

        $this->assertEquals($sum, 15);
    }

    /**
     * Test Multiplication
     * 
     * @return void
     */
    public function testCalculatorMultiplyFail()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->multiply(3);

        $this->assertNotEquals($sum, 4);
    }

    /**
     * Test And
     * 
     * @return void
     */
    public function testCalculatorAnd()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->and(2);

        $this->assertEquals($sum, true);
    }

    /**
     * Test And
     * 
     * @return void
     */
    public function testCalculatorAndFail()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->and(0);

        $this->assertNotEquals($sum, true);
    }

    /**
     * Test And
     * 
     * @return void
     */
    public function testCalculatorOr()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->or(2);

        $this->assertEquals($sum, true);
    }

    /**
     * Test And
     * 
     * @return void
     */
    public function testCalculatorXorFalse()
    {

        $calculator = new Calculator(5);
        $sum = $calculator->xor(2);

        $this->assertEquals($sum, false);
    }

    /**
     * Test And
     * 
     * @return void
     */
    public function testCalculatorXorTrue()
    {

        $calculator = new Calculator(0);
        $sum = $calculator->xor(2);

        $this->assertEquals($sum, true);
    }
}
